close all;
duration = [1.8 2.4 3.0];
n_steps = 60;
n_eps = 10*1000000;
counts = cell(3);
parfor j=1:length(duration)
    fidelities = zeros(n_eps,1);
    system = Bloch_sphere(duration(j),n_steps);
    U_1 = expm(-1i*(-system.sigma_z + 4*system.sigma_x)*system.delta_t);
    U_2 = expm(-1i*(-system.sigma_z - 4*system.sigma_x)*system.delta_t);
    U = {U_1, U_2};
    system.psi_initial = Get_Psi(system,+2,0,-1);
    system.psi_final = Get_Psi(system,-2,0,-1);
    psi = system.psi_initial;
    figure(j)
    for eps = 1:n_eps
        psi = system.psi_initial;
        for i=1:n_steps
            
            action = datasample([1,2],1);
            psi = U{action} * psi;
        end
        fidelities(eps) = abs(psi'*system.psi_final).^2;
        if(mod(eps,10000)==0)
            disp(['running from eps ' num2str(e ps)])
        end
    end
    edges = linspace(0,1,50);
    [counts{j},edges] = histcounts(fidelities,edges);
    %title(['running ' num2str(n_eps) ' random evolutions at T=' num2str(duration), ', N_{steps} = ',num2str(n_steps)]);
    %xlabel('final fidelity')
    %ylabel('frequency')
end