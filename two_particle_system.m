classdef two_particle_system
    
    properties
       Nsteps;
       dim; %dimension of system
       time; 
       psi;
       Total_time;
       psi_initial;
       psi_final;
       H1;
       H2;
       sigma_x;%pauli_matrices
       sigma_y;
       sigma_z;
       delta_t;
       psi_liste;
       psi_liste_best;
       hx_list;
       htime_list;
    end
    
    methods
        function this = two_particle_system(T,Nsteps)
            this.dim = 4;
            this.time = 0;
            this.Total_time = T;
            this.sigma_x = 1/2*[0,1;1,0];
            this.sigma_y = 1/2*[0,-1i;1i,0];
            this.sigma_z = 1/2*[1,0;0,-1];
            this.H1 = this.get_Hamiltonian(1,-4);
            this.H2 = this.get_Hamiltonian(1,+4);
            this.Nsteps = Nsteps;
            this.delta_t = this.Total_time/this.Nsteps;
            this.htime_list = linspace(0,this.Total_time,this.Nsteps);
            this.hx_list = 1*ones(1,this.Nsteps);
            
        end
        
        function H = get_Hamiltonian(this,h_z,h_x)
            H = -(kron(this.sigma_z,this.sigma_z) + ...
                h_z*( kron(this.sigma_z,eye(2)) + kron(eye(2),this.sigma_z) ) + ...
                h_x*( kron(this.sigma_x,eye(2)) + kron(eye(2),this.sigma_x) ) );
        end
        
        function psi_0 = get_ground_state(this,h_z,h_x)
            H = this.get_Hamiltonian(h_z,h_x);
            [V,D] = eig(H);
            psi_0 = V(:,1);
        end
        
        function R = reward(obj,psi,step)
            R = 0;
            if(step == obj.Nsteps)
                R = abs(psi'*obj.psi_final).^2;
            end
        end
        
        function xyz = get_spin_direction(this,psi)
            S_x1 = 2*kron(this.sigma_x,eye(2));
            S_x2 = 2*kron(eye(2),this.sigma_x);
            S_y1 = 2*kron(this.sigma_y,eye(2));
            S_y2 = 2*kron(eye(2),this.sigma_y);
            S_z1 = 2*kron(this.sigma_z,eye(2));
            S_z2 = 2*kron(eye(2),this.sigma_z);
            x1 = psi'*S_x1*psi;
            x2 = psi'*S_x2*psi;
            y1 = psi'*S_y1*psi;
            y2 = psi'*S_y2*psi;
            z1 = psi'*S_z1*psi;
            z2 = psi'*S_z2*psi;
            xyz = [x1 x2; y1 y2; z1 z2];
        end
        
        function plot_psi(this,psi,style)
            if ~exist('style','var')
                % third parameter does not exist, so default it to something
                style = '-k';
            end
            direction = this.get_spin_direction(psi);
            plot3([0 direction(1,1)],[0 direction(2,1)],[0 direction(3,1)],style,'LineWidth',3);
            plot3(2+[0 direction(1,2)],[0 direction(2,2)],[0 direction(3,2)],style,'LineWidth',3);
        end
        
        function plot_sphere(this,name)
            
            h = figure;
            hold on;
            this.plot_psi(this.psi_final,'-b');
            this.plot_psi(this.psi_initial,'-m');
            axis([-1  1  -1 1]);
            view([1,0.2,0.5]);
            [r_1,r_2,r_3] = sphere;
            surf(r_1,r_2,r_3,'FaceAlpha',0.01,'EdgeAlpha',0.5);
            [r_1,r_2,r_3] = sphere;
            surf(r_1+2,r_2,r_3,'FaceAlpha',0.01,'EdgeAlpha',0.5);
            xlabel('x','interpreter','latex','fontsize',14);
            ylabel('y');
            zlabel('z');
            axis equal 
            axis tight manual % this ensures that getframe() returns a consistent size
            direction = this.get_spin_direction(this.psi_initial);
            for int = 1:1:length(this.psi_liste_best)
                psi = this.psi_liste_best(int,:).';
                old = direction;
                direction = this.get_spin_direction(psi);
                plot3([old(1,1) direction(1,1)],[old(2,1) direction(2,1)],[old(3,1) direction(3,1)]...
                    ,'k-','LineWidth',2);
                plot3(2+[old(1,2) direction(1,2)],[old(2,2) direction(2,2)],[old(3,2) direction(3,2)]...
                    ,'k-','LineWidth',2);
                
                drawnow;
                            
                frame = getframe(h);
                im = frame2im(frame);
                [imind,cm] = rgb2ind(im,256);
                if int == 1
                    imwrite(imind,cm,name,'gif', 'Loopcount',inf,'DelayTime',0.1);
                else
                    imwrite(imind,cm,name,'gif','WriteMode','append','DelayTime',0.1);
                end
                
            end
        end
        
        function P = get_purity(this, psi)
            %Calculate density matrix
            rho = psi*(psi');
            %Calculate partial trace tr_B(rho)
            rho_A = [trace(rho(1:2,1:2)) trace(rho(1:2,3:4));...
                     trace(rho(3:4,1:2)) trace(rho(3:4,3:4))];
            %Calculate purity
            P = trace(rho_A*rho_A);
        end
        
        function Ps = get_purity_protocol(this,psi_t)
            purity = zeros(1,size(psi_t,2));
            %Calculate purity of state psi for every time step.
            for i=1:size(psi_t,2)
                purity(i) = this.get_purity(psi_t(:,i));
            end
            Ps = abs(purity);
        end

        
        
    end
    
    
    
    
    
    
    
end