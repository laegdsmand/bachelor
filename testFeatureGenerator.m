function testFeatureGenerator()
    params.t_final = 1.5;
    params.n_steps = 50;
    params.n_episodes = 50;
    ag = Agent(params);
    fg = FeatureGenerator(10,ag.system,ag);
    
    basis_dim = size(fg.psi_basis);
    assert(basis_dim(1) == 50);
    assert(basis_dim(2) == 2);
    assert(basis_dim(3) == 10);
    
    state.step = params.n_steps;
    state.psi = fg.psi_basis(state.step, :, 1).';
    x = fg.get_features(state, eye(2));
    assert(x(1) == 1,'get_feature failed to compute overlap correctly');
    
    
    w = fg.init_weights();
    assert(w(1) == fg.system.reward(fg.psi_basis(end,:,1).',fg.system.Nsteps) , 'weights guess is not equal to reward')
    disp('Test Passed :):)):):):):))))!')
end