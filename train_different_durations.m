params.t_final = 3.0;
params.n_steps = 50;
params.n_episodes = 200;
n_ts = 20;
t_f = linspace(1,3,n_ts);
max_fidelity = zeros(n_ts,1);
for i = 1:n_ts
   disp(['Training agent for t_f = ' num2str(t_f(i))])
   params.t_final = t_f(i);
   ag = Agent(params);
   Rs = ag.train();
   ag.epsilon = 0;
   Rs = ag.train();
   max_fidelity(i) = max(Rs);
   
end
plot(t_f,max_fidelity,'r.','MarkerSize',16);