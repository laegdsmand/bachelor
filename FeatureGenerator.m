%Responsible for generating features for function approximation

classdef FeatureGenerator < handle
    
    properties
       psi_basis;       % trajectories for comparison'
       n_trajectories;  % Number of trajectories in psi_basis
       system;          
       agent;
    end
    
    methods
       
        function this = FeatureGenerator(n_trajectories,system,agent)
            this.system = system;
            this.n_trajectories = n_trajectories;
            this.agent = agent;
            this.init_psi_basis();
        end
        
        function init_psi_basis(this)
            this.psi_basis = zeros(this.system.Nsteps, 2, this.n_trajectories);
            for i = 1:this.n_trajectories
                psi = this.system.psi_initial;
                this.psi_basis(1,:,i) = psi;
                for j = 2:this.system.Nsteps
                   action = datasample([1,2],1);
                   U = this.agent.U{action};
                   psi = U*psi;
                   this.psi_basis(j,:,i) = psi;
                end
            end            
        end
        
        %Returns a feature vector for the state action pair
        function x = get_features(this, state, U)
            % The next state given action corresponding to opr U is taken
            next_psi = U * state.psi;
            step = state.step;
            fidelities = abs(conj(this.psi_basis(step,1,:))*next_psi(1) + ...
                conj(this.psi_basis(step,2,:))*next_psi(2)).^2; 
            [~,F_a ] = max(fidelities);
            x = zeros(this.n_trajectories,1);
            x(F_a) = 1;
        end
        
        function w = init_weights(this)
            rewards = this.system.reward(...
                squeeze(this.psi_basis(end,:,:)) , this.system.Nsteps);
            w = rewards;
        end
        
        function dim = get_dimensions(this)
           dim = this.n_trajectories;
        end
        
        
        
        
        
        
    end
    
end