classdef Agent < handle
    properties
        t_final;    % Length of an episode
        n_steps;    % Steps in an episode
        n_episodes; % How many episodes to train agent in
        n_fe_eps;   % Number of eps of fixed epsilon
        system;     % Physical system (Bloch sphere)
        U;          % Action space defined by time evolution operators
        weights;    % Weight vector
        feature_generator;
        alpha;      % learning parameter for gradient descent
        gamma;      % discounting parameter for future rewards
        lambda;     % trace decay parameter
        epsilon;    % probability of taking random action
        min_epsilon;
    end
    
    methods
        % Initialize the agent: Set parameters and bloch sphere system
        % And Time evolution operators 
        function this = Agent(params)
            this.lambda = 1;
            this.alpha = 0.00;
            this.gamma = 1;
            this.epsilon = 0.1;
            this.n_fe_eps = params.n_fe_eps;
            this.min_epsilon = params.min_epsilon;
            this.t_final = params.t_final;
            this.n_steps = params.n_steps;
            this.n_episodes = params.n_episodes;
            system = Bloch_sphere(this.t_final,this.n_steps);
            system.psi_initial = Get_Psi(system,+2,0,-1);
            system.psi_final = Get_Psi(system,-2,0,-1);
            % Set up time evolution operators
            U_1 = expm(-1i*(-system.sigma_z + 4*system.sigma_x)*system.delta_t);
            U_2 = expm(-1i*(-system.sigma_z - 4*system.sigma_x)*system.delta_t);
            this.U = {U_1, U_2};
            this.system = system;
            
            %Feature generator
            n_trajectories = 5;
            this.feature_generator = AccumulatingFeatureGenerator(n_trajectories,system,this);
            this.weights = zeros(this.feature_generator.get_dimensions(),1);
        end
        
        function result = train(this)
            R_list = zeros(this.n_episodes,1);
            this.weights = this.feature_generator.init_weights();
            epsilons = linspace(1,0.04,this.n_episodes-this.n_fe_eps );
            for i = 1:this.n_episodes
                if i > this.n_episodes-this.n_fe_eps
                    this.epsilon = 0.04;
                else
                    this.epsilon = epsilons(i);
                end
                [R, psi_list] = this.train_episode();
                R_list(i) = R;
                index = this.feature_generator.addTrajectory(psi_list,i,R);
                if index > 0
                    this.weights(index) = R;
                end
            end
            result = R_list;
        end
        
        function [action,z] = choose_epsilon_greedy(this, q_a, z, psi,step)
            if rand() > this.epsilon
                [~,action] = max(q_a);
            else
                action = datasample([1 2],1);
                z = 0*z;
            end
            state.psi = psi; state.step = step;
            x = this.feature_generator.get_features(state,this.U{action});
            z(x) = 1;
        end
        
        function [R,psi_list] = train_episode(this)
            psi = this.system.psi_initial;
            psi_list = zeros(this.n_steps,2);
            psi_list(1,:) = psi.';
            z = zeros(size(this.weights));
            for step = 1:this.n_steps
      
                q_a = this.estimate_action_values(psi,step);
                [action,z] = this.choose_epsilon_greedy(q_a,z,psi,step);
               
                % Observe next state
                psi = this.U{action} * psi;
                psi_list(step,:) = psi.'; 
                q = q_a(action);
                
                
                R = this.system.reward(psi,step);
                
                % Updating weights
                delta = R - q;
                if(step == this.n_steps)
                    this.weights = this.weights + this.alpha*delta*z;
                else
                    % Estimate the value of new state given that best
                    % action is taken.
                    q_a = this.estimate_action_values(psi,step+1);
                    max_q_next = max(q_a);
                    delta = delta + this.gamma * max_q_next;
                    this.weights = this.weights + delta * this.alpha * z;
                    z = this.gamma * this.lambda * z;
                end
            end
        end
        
        function [R,psi_list] = train_episode_sarsa(this)
            psi = this.system.psi_initial;
            psi_list = zeros(this.n_steps,2);
            psi_list(1,:) = psi.';
            z = zeros(size(this.weights));
            %initial action
            q_a = this.estimate_action_values(psi,1);
            [action,z] = this.choose_epsilon_greedy(q_a,z,psi,1);
            for step = 1:this.n_steps
                
                
                % Observe next state and reward
                psi = this.U{action} * psi;
                psi_list(step,:) = psi.'; 
                q = q_a(action);
                
                R = this.system.reward(psi,step);
             
                % Updating weights
                delta = R - q;
                
                if(step == this.n_steps)
                    this.weights = this.weights + this.alpha*delta*z;
                else
                    % Estimate the value of new state given that best
                    % action is taken.
                    q_a = this.estimate_action_values(psi,step+1);
                    [action,z] = this.choose_epsilon_greedy(q_a,z,psi,step+1);
                    delta = delta + this.gamma * q_a(action);
                    this.weights = this.weights + delta * this.alpha * z;
                    z = this.gamma * this.lambda * z;
                end
            end
        end
        
        function q_a = estimate_action_values(this, psi, step)
            state.psi = psi; state.step = step;
            q_a = zeros(2,1);
            for action = 1:2
                x = this.feature_generator.get_features(state,this.U{action});
                %q_a(action) = this.weights.' * x;
                if(1 ~= length(max(this.weights(x))))
                    q_a(action) = 0;
                else
                    q_a(action) = max(this.weights(x));
                end
            end
            
        end
        
        function plot_best_trajectory(this,rewards)
            figure;
            xlabel('Episode number');
            ylabel('Reward')
            plot(1:100,rewards);
            [~,best] = max(rewards);
            best = best + this.feature_generator.n_trajectories
            this.system.psi_liste_best(:,1) = this.feature_generator.psi_basis(:,1,best);
            this.system.psi_liste_best(:,2) = this.feature_generator.psi_basis(:,2,best);
            this.system.plot_sphere('best.gif')
        end
        
        function R = test_policy(this,plotting)
            this.epsilon = 0;
            [R,psi_list] = this.train_episode();
            if(plotting)
                disp(['Performance fidelity is at ' num2str(R)]);
                this.system.psi_liste_best(:,1) = psi_list(:,1);
                this.system.psi_liste_best(:,2) = psi_list(:,2);
                this.system.plot_sphere('best.gif')
            end
        end
        
        
        
        
        
    end
        
        
        
end