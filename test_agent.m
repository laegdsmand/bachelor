tic
params.t_final = 3.0;
params.n_steps = 60;
params.n_episodes = 2000;
 params.alpha = 0.0;
 params.gamma = 1;
 params.lambda = 0.9;
 params.min_epsilon = 0.05;
 params.n_fe_eps = 100;
fidelity = zeros(32,1);
parfor i=1:length(fidelity)
    ag = Agent(params);
    Rs = ag.train();
    fidelity(i) = ag.test_policy(0);
    disp(['trial ' num2str(i) ' is done'])
end
toc