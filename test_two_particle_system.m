close all;
duration = [2.5,3.0,3.5,4.0];
n_steps = 60;
counts = cell(length(duration));
parfor j = 1:length(duration)
system = two_particle_system(duration(j),n_steps);
U_1 = expm(-1i*system.H1*system.delta_t);
U_2 = expm(-1i*system.H2*system.delta_t);
U = {U_1, U_2};
system.psi_initial = get_ground_state(system,1,-2);
%system.psi_initial = [1;0;0;0];
system.psi_final = get_ground_state(system,1,2);
%system.psi_final = 1/sqrt(2) * [1;0;0;1];
n_eps = 10000000;
fidelities = zeros(n_eps,1);
for eps = 1:n_eps
    psi = system.psi_initial;
    for i=1:n_steps
        
        action = datasample([1,2],1);
        psi = U{action} * psi;
    end
    fidelities(eps) = abs(psi'*system.psi_final).^2;
    if(mod(eps,10000)==0)
       disp(['running from eps ' num2str(eps)]) 
    end
end
edges = linspace(0,1,101);
[counts{j},edges] = histcounts(fidelities,edges);
end
%histogram(fidelities,edges);
%title(['running ' num2str(n_eps) ' random evolutions at T=' num2str(duration), ', N_{steps} = ',num2str(n_steps)]);
%xlabel('final fidelity')