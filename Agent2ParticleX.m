classdef Agent2ParticleX < handle
    properties
        t_final;    % Length of an episode
        n_steps;    % Steps in an episode
        n_episodes; % How many episodes to train agent in
        system;     % Physical system (Bloch sphere)
        U;          % Action space defined by time evolution operators
        weights;    % Weight vector
        feature_generator;
        alpha;      % learning parameter for gradient descent
        gamma;      % discounting parameter for future rewards
        lambda;     % trace decay parameter
        epsilon;    % probability of taking random action
    end
    
    methods
        % Initialize the agent: Set parameters and bloch sphere system
        % And Time evolution operators 
        function this = Agent2ParticleX(params)
            this.lambda = params.lambda;
            this.alpha = params.alpha;
            this.gamma = params.gamma;
            this.epsilon = 1;
            this.t_final = params.t_final;
            this.n_steps = params.n_steps;
            this.n_episodes = params.n_episodes;
            system = two_particle_system(this.t_final,this.n_steps);
            system.psi_initial = get_ground_state(system,1,-2);
            %system.psi_final = get_ground_state(system,1,2);
            system.psi_final = 1/sqrt(2)*[1;0;0;1]; %Bell state
            % Set up time evolution operators
            this.U = {expm(-1i*system.H1*system.delta_t), expm(-1i*system.H2*system.delta_t)};
            this.system = system;
            
            %Feature generator
            n_trajectories = 1000;
            this.feature_generator = FeatureGeneratorX(n_trajectories,system,this);
            this.weights = zeros(this.feature_generator.get_dimensions(),1);
        end
        
        function result = train(this)
            R_list = zeros(this.n_episodes,1);
            this.weights = this.feature_generator.init_weights();
            epsilons = linspace(1,0.02,this.n_episodes-100);
            for i = 1:this.n_episodes
                if i > this.n_episodes-100
                    this.epsilon = 0.02;
                else
                    this.epsilon = epsilons(i);
                end
                [R, psi_list] = this.train_episode();
                
                R_list(i) = R;
            end
            result = R_list;
        end
        
        function [action,z] = choose_epsilon_greedy(this, q_a, z, psi,step)
            if rand() > this.epsilon
                [~,action] = max(q_a);
            else
                action = datasample([1 2],1);
                z = 0*z;
            end
            state.psi = psi; state.step = step;
            x = this.feature_generator.get_features(state,this.U{action});
            z = z + x;
        end
        
        function [R,psi_list] = train_episode(this)
            psi = this.system.psi_initial;
            psi_list = zeros(this.n_steps,this.system.dim);
            psi_list(1,:) = psi.';
            z = zeros(size(this.weights));
            for step = 1:this.n_steps
      
                q_a = this.estimate_action_values(psi,step);
                [action,z] = this.choose_epsilon_greedy(q_a,z,psi,step);
                % Observe next state
                psi = this.U{action} * psi;
                normtest = abs(psi'*psi)^2;
                if normtest < 0.999999
                    disp('error')
                    return
                end
                psi_list(step,:) = psi.'; 
                q = q_a(action);
                
                
                R = this.system.reward(psi,step);
                
                % Updating weights
                delta = R - q;
                if(step == this.n_steps)
                    this.weights = this.weights + this.alpha*delta*z;
                else
                    % Estimate the value of new state given that best
                    % action is taken.
                    q_a = this.estimate_action_values(psi,step+1);
                    max_q_next = max(q_a);
                    delta = delta + this.gamma * max_q_next;
                    this.weights = this.weights + delta * this.alpha * z;
                    z = this.gamma * this.lambda * z;
                end
            end
        end
        
        function q_a = estimate_action_values(this, psi, step)
            state.psi = psi; state.step = step;
            q_a = zeros(2,1);
            for action = 1:2
                x = this.feature_generator.get_features(state,this.U{action});
                q_a(action) = (x')*this.weights;
            end
            
            
        end
        
        function R = test_policy(this,plotting)
            this.epsilon = 0;
            [R,psi_list] = this.train_episode();
            if(plotting)
                disp(['Performance fidelity is at ' num2str(R)]);
                this.system.psi_liste_best = psi_list;
                this.system.plot_sphere('best.gif');
            end
        end
        
        
        
        
        
    end
        
        
        
end