%Responsible for generating features for function approximation

classdef ReducedFeatureGenerator < handle
    
    properties
       psi_basis;       % trajectories for comparison
       n_trajectories;  % Number of trajectories in psi_basis
       system;
       dimension;
       agent;
    end
    
    methods
       
        function this = ReducedFeatureGenerator(n_trajectories,system,agent)
            this.system = system;
            this.n_trajectories = n_trajectories;
            this.agent = agent;
            this.init_psi_basis();
        end
        
        function init_psi_basis(this)
            this.psi_basis = zeros(this.system.Nsteps, this.system.dim, this.get_dimensions());
            for i = 1:this.n_trajectories
                psi = this.system.psi_initial;
                for j = 1:this.system.Nsteps
                   action = datasample([1,2],1);
                   U = this.agent.U{action};
                   psi = U*psi;
                   this.psi_basis(j,:,i) = psi;
                end
            end            
        end
        
        %Returns a feature vector for the state action pair
        function x = get_features(this, state, U)
            % The next state given action corresponding to opr U is taken
            next_psi = U * state.psi;
            step = state.step;
            fidelities = zeros(this.get_dimensions(),1);
            for i = 1:this.system.dim
            fidelities = fidelities + reshape(conj(this.psi_basis(step,i,:)),this.get_dimensions(),1).*next_psi(i);
            end
            fidelities = abs(fidelities).^2;
            
            F_a = fidelities >= 0.995;
            %x = zeros(this.get_dimensions,1);
            x = F_a;
        end
        
        function w = init_weights(this)
            rewards = this.system.reward(...
                squeeze(this.psi_basis(end,:,:)) , this.system.Nsteps);
            w = rewards;
        end
        
        function dim = get_dimensions(this)
           dim = this.n_trajectories;
        end
        
        function index = addTrajectory(this,psi_list,episode,R)
             mean_fidelity = zeros(this.n_trajectories,1);
             for i=1:this.n_trajectories
                 trajectory =  this.psi_basis(:,:,i);
                 fidelities = sum(conj(psi_list).*trajectory,2);
                 fidelities = abs(fidelities).^2;
                 mean_fidelity(i) = mean(fidelities);
             end
            [min_w, index] = min(this.agent.weights);
            if(max(mean_fidelity)>0.999999)
                index = 0;
                return;
            end
            if R > min_w
                this.psi_basis(:,:,index) = psi_list;
            else
                index = 0;
            end
        end
        
        
        
        
        
        
    end
    
end