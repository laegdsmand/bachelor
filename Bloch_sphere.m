classdef Bloch_sphere < handle
    properties
       Nsteps;
       time; 
       dim; % dimension of system
       psi; % vector of dimension 2
       Total_time;
       psi_initial;
       psi_final;
       Hamiltonian;
       Fidelity;
       sigma_x;%pauli_matrices
       sigma_y;
       sigma_z;
       delta_t;
       psi_liste;
       psi_liste_best;
       hx_list;
       htime_list;
    end
    
    methods
        function obj = Bloch_sphere(T,Nsteps)
            obj.dim = 2;
            obj.time = 0;
            obj.Total_time = T;
            obj.sigma_x = 1/2*[0,1;1,0];
            obj.sigma_y = 1/2*[0,-1i;1i,0];
            obj.sigma_z = 1/2*[1,0;0,-1];
            obj.Nsteps = Nsteps;
            obj.delta_t = obj.Total_time/obj.Nsteps;
            obj.htime_list = linspace(0,obj.Total_time,obj.Nsteps);
            obj.hx_list = 1*ones(1,obj.Nsteps);
        end
        
        function psi_0 = Get_Psi(obj,x,y,z)
            % Here you get a groundstate of the Hamiltonian
            H = x.*obj.sigma_x + y.*obj.sigma_y + z.*obj.sigma_z;
            [V,D] = eig(H);
            psi_0 = V(:,1);
        end
        



        
        function R = reward(obj,psi,step)
            R = 0;
            if(step == obj.Nsteps)
                R = abs(psi'*obj.psi_final).^2;
            end
        end
        
        function xyz = get_spin_direction(this,psi)
            x = 2*psi'*this.sigma_x*psi;
            y = 2*psi'*this.sigma_y*psi;
            z = 2*psi'*this.sigma_z*psi;
            xyz = [x;y;z];
        end
        
        function plot_sphere(obj,name)
            
            h = figure;
            hold on;
            x = 2*obj.psi_final'*obj.sigma_x*obj.psi_final;
            y = 2*obj.psi_final'*obj.sigma_y*obj.psi_final;
            z = 2*obj.psi_final'*obj.sigma_z*obj.psi_final;
            Marker2 = plot3(x,y,z,'LineWidth',3,'Color',[129, 55, 114]/255);
            
            x_init = 2*obj.psi_initial'*obj.sigma_x*obj.psi_initial;
            y_init = 2*obj.psi_initial'*obj.sigma_y*obj.psi_initial;
            z_init = 2*obj.psi_initial'*obj.sigma_z*obj.psi_initial;
            Marker = plot3(x_init,y_init,z_init,'LineWidth',3,'Color',[184, 38, 1]/255);
               x = x_init; 
               y = y_init; 
               z = z_init;
    
            
            
            axis([-1  1  -1 1]);
            view([1,0.2,0.5]);
            [r_1,r_2,r_3] = sphere;
            surf(r_1,r_2,r_3,'FaceAlpha',0.01,'EdgeAlpha',0.5);
            xlabel('x');
            ylabel('y');
            zlabel('z');
            axis equal 
            axis tight manual % this ensures that getframe() returns a consistent size
            filename = [name];
%             x_old = x;
%             y_old = y;
%             z_old = z;
            bloch_trajectory = zeros(length(obj.psi_liste_best),3);
            for int = 1:1:length(obj.psi_liste_best)
                psi = [obj.psi_liste_best(int,1); obj.psi_liste_best(int,2)];
                x_old = x;
                y_old = y;
                z_old = z;
                x = 2*psi'*obj.sigma_x*psi;
                y = 2*psi'*obj.sigma_y*psi;
                z = 2*psi'*obj.sigma_z*psi;
                set(Marker,'XData',[0 x_init]);
                set(Marker,'YData',[0 y_init]);
                set(Marker,'ZData',[0 z_init]);
                bloch_trajectory(int,:) = [x y z];
                plot3([x_old x],[y_old y],[z_old z],'k-','LineWidth',2,'Color',[6, 47, 79]/255);
                
                x_fin = 2*obj.psi_final'*obj.sigma_x*obj.psi_final;
                y_fin = 2*obj.psi_final'*obj.sigma_y*obj.psi_final;
                z_fin = 2*obj.psi_final'*obj.sigma_z*obj.psi_final;
                set(Marker2,'XData',[0 x_fin]);
                set(Marker2,'YData',[0 y_fin]);
                set(Marker2,'ZData',[0 z_fin]);
                drawnow;
                
                frame = getframe(h);
                im = frame2im(frame);
                [imind,cm] = rgb2ind(im,256);
                if int == 1
                    imwrite(imind,cm,filename,'gif', 'Loopcount',inf,'DelayTime',0.1);
                else
                    imwrite(imind,cm,filename,'gif','WriteMode','append','DelayTime',0.1);
                end
                
                %pause(0.1);
            end
            
            
        end
        
    end
    
    
    
end