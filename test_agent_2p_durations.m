
n_time_steps = 32;
n_trials = 16;
time = linspace(0.2,4,n_time_steps);
fidelity = zeros(n_time_steps,1);
params = cell(n_time_steps,1);
parfor i = 1:n_time_steps
    disp(['testing for duration = ' num2str(time(i))]);
    params{i}.n_steps = 60;
    params{i}.n_episodes = 5000;
    params{i}.alpha = 0;
    params{i}.gamma = 1;
    params{i}.lambda = 0.9;
    params{i}.t_final = time(i);
    best = 0;
    for j = 1:n_trials
        disp(['training agent for T = ' num2str(time(i))]);
        ag = Agent2Particle(params{i});
        ag.train();
        this_fidelity = ag.test_policy(0);
        if this_fidelity > best
            best = this_fidelity;
        end
        fidelity(i) = best;
    end
    
end
plot(time,fidelity, 'b.','MarkerSize',8);
title('Being greedy after 1000 training episodes (best of 5)');
xlabel('T_{final}');
ylabel('Fidelity');