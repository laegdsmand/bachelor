tic
params.t_final = 4.4;
params.n_steps = 60;
params.n_episodes = 2000;
params.alpha = 0.01;
params.gamma = 1;
params.lambda = 0.95;
params.n_fe_eps = 400;
params.min_epsilon = 0.04;
% params.alpha = 0.05;
% params.gamma = 1;
% params.lambda = 0.9;
fidelity = zeros(64,1);
parfor i=1:length(fidelity)
    ag = Agent2Particle(params);
    Rs = ag.train();
    fidelity(i) = ag.test_policy(0); 
    disp(['trial ' num2str(i) ' is done'])
end
toc