n_trials = 64;
fidelity = zeros(n_trials,1);
feps = [0 20 40 60 80 100];
epsilon = [0.04 0.1 0.15 0.2 0.25];
Z = zeros(length(feps),length(epsilon));
stderr =  zeros(length(feps),length(epsilon));
params.t_final = 2.4;
params.n_steps = 60;
params.n_episodes = 100;
params.alpha = 0;
params.gamma = 0.99;
params.lambda = 0.95;
for a = 1:length(feps)
    params.n_fe_eps = feps(a);
    for l = 1:length(epsilon)
        params.min_epsilon = epsilon(l);
        fidelity = zeros(size(fidelity));
        parfor i=1:length(fidelity)
            ag = Agent(params);
            Rs = ag.train();
            fidelity(i) = ag.test_policy(0);
            disp(['trial ' num2str(i) ' is done'])
        end
        disp(['testing agent for alpha = ' num2str(feps(a)) ' and lambda = ' num2str(epsilon(l))])
        Z(a,l) = (mean(fidelity));
        stderr(a,l) = std(fidelity)/length(fidelity);
        
    end
end
bar3(Z);

