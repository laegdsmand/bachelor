%Responsible for generating features for function approximation

classdef AccumulatingFeatureGenerator < handle
    
    properties
       psi_basis;       % trajectories for comparison
       n_trajectories;  % Number of trajectories in psi_basis
       system;
       dimension;
       agent;
    end
    
    methods
       
        function this = AccumulatingFeatureGenerator(n_trajectories,system,agent)
            this.system = system;
            this.n_trajectories = n_trajectories;
            this.agent = agent;
            this.init_psi_basis();
        end
        
        function init_psi_basis(this)
            this.psi_basis = zeros(this.system.Nsteps, this.system.dim, this.n_trajectories + this.agent.n_episodes);
            for i = 1:this.n_trajectories
                psi = this.system.psi_initial;
                for j = 1:this.system.Nsteps
                   action = datasample([1,2],1);
                   U = this.agent.U{action};
                   psi = U*psi;
                   this.psi_basis(j,:,i) = psi;
                end
            end            
        end
        
        %Returns a feature vector for the state action pair
        function x = get_features(this, state, U)
            % The next state given action corresponding to opr U is taken
            next_psi = U * state.psi;
            step = state.step;
            fidelities = zeros(this.get_dimensions(),1);
            for i = 1:this.system.dim
                fidelities = fidelities + reshape(conj(this.psi_basis(step,i,:)),this.get_dimensions(),1).*next_psi(i);
            end
            fidelities = abs(fidelities).^2;
            F_a = fidelities > 0.995; %max(fidelities)-0.0001;
            x = zeros(this.n_trajectories + this.agent.n_episodes,1);
            x = F_a;
        end
        
        function w = init_weights(this)
            rewards = this.system.reward(...
                squeeze(this.psi_basis(end,:,:)) , this.system.Nsteps);
            w = rewards;
        end
        
        function dim = get_dimensions(this)
           dim = this.n_trajectories+this.agent.n_episodes;
        end
        
        function index = addTrajectory(this,psi_list,episode,R)
            this.psi_basis(:,:,this.n_trajectories+episode) = psi_list;
            index = this.n_trajectories + episode;
        end
        
        
        
        
        
        
    end
    
end