n_trials = 128;
fidelity = zeros(n_trials,1);
feps = [100 200 300 400 500 600];
epsilon = [0.01 0.03 0.04 0.05 0.06 0.07];
Z = zeros(length(feps),length(epsilon));
stderr =  zeros(length(feps),length(epsilon));
params.t_final = 3.5 ;
params.n_steps = 60;
params.n_episodes = 1000;
params.alpha = 0;
params.gamma = 0.99;
params.lambda = 0.95;
for a = 1:length(feps)
    params.n_fe_eps = feps(a);
    for l = 1:length(epsilon)
        params.min_epsilon = epsilon(l);
        fidelity = zeros(size(fidelity));
        parfor i=1:length(fidelity)
            ag = Agent2Particle(params);
            Rs = ag.train();
            fidelity(i) = ag.test_policy(0);
            disp(['trial ' num2str(i) ' is done'])
        end
        disp(['testing agent for alpha = ' num2str(feps(a)) ' and lambda = ' num2str(epsilon(l))])
        Z(a,l) = (mean(fidelity));
        stderr(a,l) = std(fidelity)/length(fidelity);
        0.
    end
end
bar3(Z);

