params.n_steps = 60;
params.n_episodes = 400;
n_time_steps = 32;
n_trials = 24;
time = linspace(0.05,2.9,58);
n_time_steps = length(time);
fidelity = zeros(n_time_steps,1);
for i = 1:n_time_steps
    params.t_final = time(i);
    best = 0;
    for j = 1:n_trials
        disp(['training agent for T = ' num2str(time(i))]);
        ag = Agent(params);
        ag.train();
        this_fidelity = ag.test_policy(0);
        if this_fidelity > best
            best = this_fidelity;
        end
        fidelity(i) = best;
    end
end
plot(time,fidelity, 'b.','MarkerSize',8);
title('Being greedy after 100 training episodes');
xlabel('Protocol duration, T');
ylabel('Fidelity (\psi(T),\psi_{target})');